import flask
from product import ProductType, Product
from flask import Flask

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World"


@app.route("/products/")
def products():

    response = flask.jsonify(
        {
            "products": [
                Product(ProductType.Digital, "Digital 1", 0, 0, False).to_json(),
                Product(ProductType.Digital, "Digital 2", 0, 0, True).to_json(),
                Product(ProductType.Package, "Package 1", 5, 0, False).to_json(),
                Product(ProductType.Package, "Package 2", 10, 0, True).to_json(),
                Product(ProductType.Promotional, "Promo - Discounted Product", 0, 50, False).to_json(),
                Product(ProductType.Promotional, "Promo - Free product", 0, 0, True).to_json(),
            ],
        }        
    )
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
