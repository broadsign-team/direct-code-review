# direct-code-review

A member of your team has pushed some code and asked you to review it. Take some time to review the code.

During the interview, you will be asked to suggest some improvements to the existing code. Make sure to refresh
your memory on SOLID principles!

You will also be asked to refactor some code and/or add some new functionalities.

Make sure you can run the project in your local machine and have fun!

## Backend

To run the backend:

1. Navigate to the backend folder in your terminal `cd backend` if you're at the root folder. 
2. Build the docker image `docker build . -t direct-code-review-backend`
3. Run the container with our newly built image `docker run -it --init -p 5000:5000 -v $(pwd):/usr/src/app direct-code-review-backend`
4. Navigate to `http://0.0.0.0:5000/`
5. If you see `Hello World`, the app is running.


To test that everything works, you can change `Hello World` to `Hello World from docker!!`,
reload the page in your browser, and you should see the changes.

## Frontend

To run the frontend:

1. Navigate to the frontend folder in your terminal `cd frontent` if you're at the root folder.
2. `npm install` if it's the first time you're running it
3. `npm start` 
